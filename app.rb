require 'bundler'
Bundler.require

require './helpers'
require './model'
require './routes/passenger'
require './routes/cab'